export enum ApplicationStatus {
  READY, RUNNING, FINISHED
}
