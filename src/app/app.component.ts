import {Component} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ApplicationStatus} from './ApplicationStatus';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  questions: any[];
  results: any[];
  status: ApplicationStatus = ApplicationStatus.READY;
  statusType: any = ApplicationStatus;
  currentQuestion = 0;
  temp = -1;

  constructor(private http: HttpClient) {
    this.loadQuestions();
    this.loadResults();
  }

  private loadQuestions() {
    this.http.get('/assets/json/questions.json', {responseType: 'text'})
      .subscribe(data => {
        this.questions = JSON.parse(data);
      });
  }

  private loadResults() {
    this.http.get('/assets/json/results.json', {responseType: 'text'})
      .subscribe(data => {
        this.results = JSON.parse(data);
      });
  }

  isLastQuestion(): boolean {
    return this.questions.length === this.currentQuestion + 1;
  }

  confirmAnswer() {
    if (this.temp !== -1) {
      this.questions[this.currentQuestion++].chose = this.temp;
      this.temp = -1;
    }
  }

  switchToFinished() {
    if (this.temp !== -1) {
      this.status = ApplicationStatus.FINISHED;
      this.confirmAnswer();
    }
  }

  countScore(): number {
    let totalScore = 0;
    for (const question of this.questions) {
      totalScore = totalScore + question.variants[question.chose].value;
    }
    return totalScore;
  }

  choseRadio(i: number) {
    this.temp = i;
  }

  getResult(score: number): string {
    let resultForUser = null;
    for (const result of this.results) {
      if (score >= result.min && score < result.max) {
        resultForUser = result.text;
        break;
      }
    }

    return resultForUser;
  }

  repeatTest() {
    this.temp = -1;
    this.currentQuestion = 0;
    this.status = ApplicationStatus.RUNNING;
  }

  getCorrectAnswer(variants: any[]): string {
    let correctAnswer = variants[0];

    for (const variant of variants) {
      if (variant.value > correctAnswer.value) {
        correctAnswer = variant;
      }
    }

    return correctAnswer.text;
  }
}
